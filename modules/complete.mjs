import { hamburgerButton,mobileNav,mygitlaburl,mygitlabrepos} from "./axios-utils.mjs";
import { header_computed_distance,bpms } from './miscellaneous-variables.mjs';
import { RetPropertyRoutine } from './utility-functions.mjs';
import { myPresentYear, datestamp } from './copyright-function.mjs';

const masterBranch = (function (){
  
  "use strict";

  /* #########
  ### private variables
  ######### */
  
  // console.log( "axios: " , axios ); // function
  
  let loopTimer_elev;

  let _RAF;
  let _CAF;

  let _tgtELId_elev; 
  let _tgtEl_elev; 

  let currentY_elev; 
  let targetY_elev;

  // console.log( "string_padtop_header" , string_padtop_header );
  // console.log( "string_padbtm_header" , string_padbtm_header );
  // console.log( "header_computed_distance" , header_computed_distance );

  /* #########
  ### private helper functions
  ######### */
  
  const domconlo = () => { // console.log( "le dcl" ); 
    hamburgerButton.addEventListener("click", newOpen, false);
    mobileNav.addEventListener("click", newClose, false); 
    datestamp.innerText = `\u00A9 ${myPresentYear}`;
  };

  const load = () => { // console.log( "le load" );
    
  const allpromises = fetchgitlabdata();
    
    allpromises
    .then( spreaddata )
    .then( printdata )
    .catch(thrown);

  };

  const fetchgitlabdata = async () => { 
    // headers: { 'Authorization': `Bearer BlablaBlahhhhHHHhhhhHHH` } // ice..
    const res = await axios.all( [ axios.get(mygitlaburl) , axios.get(mygitlabrepos) ] );
    return res;
  };

  const newOpen = () => { mobileNav.classList.add("open"); }
  const newClose = () => { mobileNav.classList.remove("open"); }

  const thrown = (error) => {
    console.log( "Error: " , error ); // Error: "Network Error"
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      console.log( "error.response.data" , error.response.data );
      console.log( "error.response.status" , error.response.status );
      console.log( "error.response.headers" , error.response.headers );
    } 
    else 
    if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      console.log( "error.request" , error.request );
    } 
    else {
      // Something happened in setting up the request that triggered an Error
      console.log('Error - error.message ', error.message );
    }
    console.log( "error.config " , error.config );
  };

  const spreaddata = axios.spread( (userres, repores ) => {
    const newObj = { user: userres.data , repos: repores.data };
    return newObj; 
  } );

  const printdata = async ( object ) => {
    const { user, repos } = await object; 
    // console.log( "user", user );
    printBio( user ); 
    // console.log( "repos" , repos );
    printRepos( repos );

    generateSidePaneLinks( repos );
    generateHeaderLinks( repos ); 
  };

  async function printBio( arr ){
    
    // console.log( "arr[0].avatar_url: " , arr[0].avatar_url );
    // console.log( "arr[0].id: " , arr[0].id );
    // console.log( "arr[0].name: " , arr[0].name );
    // console.log( "arr[0].state: " , arr[0].state );
    // console.log( "arr[0].username: " , arr[0].username );
    // console.log( "arr[0].web_url: " , arr[0].web_url );
    
    const { avatar_url, name, username, web_url } = await arr[0];
    
    const userhtml = `<h1 class="imageheader" style="background-image: url(${avatar_url});" id="gitlabuser">${name} (aka. ${username})</h1>`; 
    const detailshtml = `<div id="gitlabuserdetails">
      <p>Visit <a href="${web_url}" target="_blank">gitlab.com/${username}</a></p>
    </div>`;

    // document.querySelector("#gitlabuser").insertAdjacentText("beforeend", userhtml );
    // document.querySelector("#gitlabuserdetails").insertAdjacentHTML("beforeend" , detailshtml ); 
    document.querySelector("#gitlabdetails").insertAdjacentHTML( "beforeend", userhtml );
    document.querySelector("#gitlabdetails").insertAdjacentHTML( "beforeend", detailshtml );
    
  }

  async function printRepos( arr ){
    
    // console.log( "arr.length" , arr.length ); // 46
    // console.log( "arr" , arr ); 

    // const firstinstance = arr[0];
    // console.log( "firstinstance" , firstinstance ); 

    const htwofrag = document.createDocumentFragment();
    const hoteltwo = document.createElement("h2");
    hoteltwo.setAttribute( "id" , "repoheader" ); 
    hoteltwo.setAttribute( "class" , "repoheader" ); 
    hoteltwo.appendChild( document.createTextNode("The Repositories") );
    
    htwofrag.appendChild( hoteltwo );
    document.querySelector("#repolanding").appendChild( htwofrag );
    // document.querySelector("#repolanding").insertAdjacentElement( "beforeend", hoteltwo );

    const uniformlima = document.createElement("ul");
    const ulfrag = document.createDocumentFragment();
    uniformlima.setAttribute("id", "gitlabrepositories");

    for( const repo of arr ){

      // console.log( "repo.name" , repo.name );
      // console.log( "arr" , arr );
      
      const dateobj = new Date(repo.created_at);
      const month = dateobj.getMonth();
      const date = dateobj.getDate();
      const year = dateobj.getFullYear();
      const datestring = `${month}/${date}/${year}`;
      
      const lihtml = await `
      <li>
        <div>
          <h3 id="${repo.name}">${repo.name}</h3>
          <p>${repo.description} (created on <code>${datestring}</code>)</p>
          <p>Visit repository named <a href="${repo.web_url}" target="_blank" rel="noopener noreferrer" alt="Link to ${repo.name}">${repo.name}</a></p>
          <p><a data-name="header" class="upTown" href="" alt="Link back to the top of the page">Back to the top</a></p>
        </div>
      </li>`;

      uniformlima.insertAdjacentHTML("beforeend" , lihtml ); 
    }

    ulfrag.appendChild( uniformlima );
    document.querySelector("#repolanding").appendChild( ulfrag );
    
    upanddown();

  }

  async function generateSidePaneLinks( arr ) {
    // console.log( "arr.length" , arr.length ); // 46
    // console.log( "arr" , arr ); 

    // const firstinstance = arr[0];
    // console.log( "generateSidePaneLinks firstinstance" , firstinstance ); 

    const ul = document.querySelector("#desktopnavigation");
    // console.log( "ul" , ul );

    if( !!(window.matchMedia !== "undefined") ){
      
      if( (arr.length <= 7) && (window.matchMedia("(min-width: 40rem)").matches ) ){
        
        // console.log( "arr.length LTE 10" , arr.length );
        
        ul.style.display="contents"; // ul.style.display="inline-flex";
      }
      
      else
      
      if( (arr.length > 7) && (window.matchMedia("(min-width: 40rem)").matches ) ){
        
        // console.log( "arr.length GT 10" , arr.length );
        
        ul.style.display="inherit";
      }
    } 
    
    for( const repo of arr ){
      const lihtml = await `<li><a id="${repo.name}_B" class="downTown" href="">${repo.name}</a></li>`;
      ul.insertAdjacentHTML("beforeend", lihtml );
    }
  }

  async function generateHeaderLinks( arr ) {
    // console.log( "arr.length" , arr.length ); // 46
    // console.log( "arr" , arr ); 

    // const firstinstance = arr[0];
    // console.log( "generateHeaderLinks firstinstance" , firstinstance ); 

    const ul = document.querySelector("#mobileitems");
    // console.log( "ul" , ul );

    for( const repo of arr ){
      const lihtml = await `<li class="mobileitem"><a id="${repo.name}_A" class="downTown" href="">${repo.name}</a></li>`;
      ul.insertAdjacentHTML("beforeend", lihtml );
    }
  }

  async function upanddown() {
    
    const toArray = (arr) => {
      return Array.prototype.slice.call(arr);
    }

    const allAnchors = await document.getElementsByTagName("a"); // HTMLCollection
    // const allAnchors = await document.querySelectorAll("a"); // NodeList
    // console.log( "allAnchors" , allAnchors );
    // console.log( "typeof allAnchors" , typeof allAnchors );
    // console.log( "Array.isArray( allAnchors )" , Array.isArray( allAnchors ) ); 

    const AnchorLength = await allAnchors.length; // console.log( "AnchorLength" , AnchorLength );

    for( let A = 0; A < AnchorLength; A = await A + 1 ){
      
      allAnchors[A].addEventListener( "click" , (evt) => { 
        if( (evt.target.className === "downTown" || evt.target.className === "upTown") && evt.target.nodeName.toLowerCase() === "a" ){
          // console.log( "evt.target", evt.target );
          evt.preventDefault();
        }
      }, false ); 
    }

    const Anchors = toArray( allAnchors ); // console.log( "Anchors" , Anchors ); 
    Anchors.forEach( initAscendDescend );
  };

  const initAscendDescend = (anchor,idx,arr) => { 
    anchor.addEventListener( "mousedown" , (evt) => {

      if (evt.target.className === "downTown" && evt.target.nodeName.toLowerCase() === "a") {
        const targetElId = evt.target.id.slice(0, evt.target.id.length - 2);
        GetDownOnIt(targetElId);
      }
      else
      if (evt.target.className === "upTown" && evt.target.nodeName.toLowerCase() === "a") {
        const targetElId = evt.target.dataset.name; 
        UpUpAndAway(targetElId);
      }

    }, false );
  }

  const GetDownOnIt = (evtTrgt) => {
    
    _RAF = RetPropertyRoutine( "requestAnimationFrame" , window );
    _CAF = RetPropertyRoutine( "CancelAnimationFrame" , window );
    
    _tgtELId_elev = evtTrgt; 
    _tgtEl_elev = document.getElementById(_tgtELId_elev);
      
    var bodyHeight_elev = document.body.offsetHeight;
    
    let yHeight_elev = window.innerHeight; 
    currentY_elev = window.pageYOffset; 
    let yPos_elev = currentY_elev + yHeight_elev; 
    
    targetY_elev = _tgtEl_elev.offsetTop;
  
    
    /**/
    // new CORE CONTROL STRUCTURE
    
    if( window[ _RAF ] ){      
      loopTimer_elev = window[ _RAF ]( function() {
        return GetDownOnIt( _tgtELId_elev );
      });     
    }
    else{     
      loopTimer_elev = window.setTimeout( function() {
        return GetDownOnIt( _tgtELId_elev );
      } , bpms );     
    }
    
    // new CORE CONTROL STRUCTURE 
    if( currentY_elev > targetY_elev ) { /* if( yPos_elev > bodyHeight_elev ) { */
      if( window[ _CAF ] ){  
        loopTimer_elev = (function( self ) {
          return window[ _CAF ]( loopTimer_elev );
        })(loopTimer_elev);
      }
      else{       
        loopTimer_elev = (function( self ) {
          return clearTimeout( loopTimer_elev );
        })(loopTimer_elev);
      } 
    }
    else {
      if( currentY_elev < targetY_elev - header_computed_distance ) {
        const _scrollY_elev = currentY_elev + header_computed_distance;
        window.scroll( 0 , _scrollY_elev );
      }
      else {
        if( window[ _CAF ] ){
          
          loopTimer_elev = (function( self ) {
            return window[ _CAF ]( loopTimer_elev );
          })(loopTimer_elev);
        }
        else{         
          loopTimer_elev = (function( self ) {
            return clearTimeout( loopTimer_elev );
          })(loopTimer_elev);
        } 
      }
      
    } // END new CORE CONTROL STRUCTURE
    /**/
    
  } // END function

  const UpUpAndAway = (evtTrgt) => { 
    
    _RAF = RetPropertyRoutine( "requestAnimationFrame" , window );
    _CAF = RetPropertyRoutine( "CancelAnimationFrame" , window );
    
    _tgtELId_elev = evtTrgt; 
    _tgtEl_elev = document.getElementById(_tgtELId_elev); 
    
    currentY_elev = window.pageYOffset;       
    targetY_elev = _tgtEl_elev.offsetTop;
    
    // new CORE CONTROL STRUCTURE
    /**/
    if( window[ _RAF ] ){      
      loopTimer_elev = window[ _RAF ]( function() {
        return UpUpAndAway( _tgtELId_elev );
      });     
    }
    else{
      loopTimer_elev = window.setTimeout( function() {
        return UpUpAndAway( _tgtELId_elev );
      } , bpms );
    }
    
    // new CORE CONTROL STRUCTURE
    if( currentY_elev > targetY_elev ) {
      const _scrollY_elev = currentY_elev - header_computed_distance;
      window.scroll( 0 , _scrollY_elev );
    }
    else { 
      if( window[ _CAF ] ){
        loopTimer_elev = (function( self ) {
          return window[ _CAF ]( loopTimer_elev );
        })(loopTimer_elev);
      }
      else {        
        loopTimer_elev = (function( self ) {
          return clearTimeout( loopTimer_elev );
        })(loopTimer_elev);
      } 
    } // END new CORE CONTROL STRUCTURE
    /**/
  } // END function  
  
  /* #########
  ###  public variables and accessor functions
  ######### */ 
  return {
    
    domconlo: (domconlo) 
    , load: function() { 
      loopTimer_elev = null;
      return load; 
    }
    
  };
} )();

window.addEventListener( "DOMContentLoaded", masterBranch.domconlo(), false );
window.addEventListener( "load", masterBranch.load(), false );