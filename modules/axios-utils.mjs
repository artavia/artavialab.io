const hamburgerButton = document.querySelector(".hamburgerbutton");
const mobileNav = document.querySelector(".mobile");
const gitlabuser = "artavia";
const gitlabbaseurl = 'https://gitlab.com/api/v4/'; // https://docs.gitlab.com/ce/api/users.html 
const mygitlaburl = `${gitlabbaseurl}users?username=${gitlabuser}`;
const repoparams = `?per_page=100&page=1&order_by=updated_at`; // https://docs.gitlab.com/ce/api/projects.html#list-user-projects
const mygitlabrepos = `${gitlabbaseurl}users/${gitlabuser}/projects${repoparams}`; 

export {
  hamburgerButton
  ,mobileNav
  ,mygitlaburl
  ,mygitlabrepos
};