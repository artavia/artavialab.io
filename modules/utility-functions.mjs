import { arVendorPREs } from './miscellaneous-variables.mjs';

const RetPropertyRoutine = ( pm1, pm2 ) => {
  const ar_vendorPreez = arVendorPREs; // [ "moz", "ms", "webkit", "o" ]  --> // object
  const clCharMax = ar_vendorPreez.length; 
  let leProp;
  let dL;
  const param = pm1; // getUserMedia
  const paramEl = pm2; // Navigator
  // const len = param.length; 
  const nc = param.slice( 0,1 ); // g
  const Uc = param.slice( 0,1 ).toUpperCase(); // G
  const Param = param.replace( nc, Uc ); // GetUserMedia  
  if ( param in paramEl ) { 
    leProp = param; 
  } 
  for ( dL = 0; dL < clCharMax; dL = dL + 1) { 
    if ( ar_vendorPreez[ dL ] + Param in paramEl ) { 
      leProp = ar_vendorPreez[ dL ] + Param; 
    } 
  } 
  return leProp;
}

export {RetPropertyRoutine};