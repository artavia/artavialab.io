const headingEL = document.querySelector("header.header");

// ~ ELEVATOR LINK functionality ~ upTown\downTown
const string_padtop_header = document.defaultView.getComputedStyle( headingEL, "").getPropertyValue( "padding-top" ); // 19.9167px

const string_padbtm_header = document.defaultView.getComputedStyle( headingEL, "").getPropertyValue( "padding-bottom" ); // 19.9167px

const num_padtop_header = parseInt( string_padtop_header.match(/[0-9.]/gi).join("") , 10 ); 
const num_padbtm_header = parseInt( string_padbtm_header.match(/[0-9.]/gi).join("") , 10 );
const header_computed_distance = num_padtop_header + num_padbtm_header ; // header_computed_distance = 40; 


const bpms = 15.46875;
const arVendorPREs = [ "moz", "ms", "webkit", "o" ];

export {header_computed_distance,bpms,arVendorPREs};