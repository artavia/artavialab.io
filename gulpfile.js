const gulp = require("gulp");
const HubRegistry = require("gulp-hub");

const hub = new HubRegistry( [
  "./dev.js"
  , "./prod.js"
] );

gulp.registry(hub);