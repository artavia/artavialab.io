# Description
Gitlab.io intro page by Luis.

## Welcome to artavia.gitlab.io
Hello there. 

This is my gitlab.io front&#45;facing template.

Enjoy your experience.

Have a nice day.

## Installation directions
You have two alternatives: a) delete yarn.lock, then, run **npm install**; or, b) simply run **yarn install** in order to get started.

## Gulp4 directions
Simply run **gulp** in order to get started. You can run **gulp &#45;T** in order to see a tree of the subdirectives that are built&#45;in, too. 


## Updated on October 10, 2020
Some changes to the **gulpfile.js**, **package.json** and **.gitlab-ci.yml** were necessary including:
  - the inclusion of **gulp-hub** was thrown into the mix in order to split out production versus development tasks. Basically, I want to remove any **pre-compiled** and/or contrived/anticipatory/choreographed set-up so that **continuous integration/continuous development** is brought to the forefront. 
  - the **.gitlab-ci.yml** file has to be completely re-worked. I got the idea from the **.gitlab-ci.yml** that is [found in a vue project I had put together recently](https://gitlab.com/artavia/confia_en_mi/-/blob/master/.gitlab-ci.yml "link to vue project"). Every cached item had to be removed save for the **package.json** and **.gitlab-ci.yml**, then, be built up again from the floor up. 


## Updated on October 7, 2020
Some changes to the **gulpfile.js** and **package.json** were necessary including:
  - Ensured that where applicable either &quot;@next&quot; or &quot;@latest&quot; or &quot;@nightly&quot; versions are utilized even if it breaking changes are introduced;
  - Added **postcss** to the mix since **gulp-postcss** was upgraded, therefore, the latter is current;
  - Utilizing **gulp-imagemin@^6.0.0** since any version greater than 6.0.0 inevitably explodes;

## Updated on July 5, 2019
Some changes to the **gulpfile.js** and **package.json** were necessary including:
  - Replaced **gulp-ruby-sass** with **gulp-sass** considering that the former is now deprecated;

## Updated on July 4, 2019
Some changes to the **gulpfile.js** and **package.json** were necessary including:
  - Using **cssnano** in lieu of **gulp-cssnano** for security reasons;
  - Using **autoprefixer** in lieu of **gulp-autoprefixer** as a result of the new issue;
  - More information on [how to address the security concerns](https://github.com/svg/svgo/issues/1119 "how to address the security concerns") is available;
  - Adding **gulp-postcss** to the workflow;
  - Adding **rollup** as a peer dependency due to inherent changes made to **gulp-better-rollup**.


## Updated on October 17, 2018
This front facing page now includes several new features including: 
  - **Axios** promise&#45;based functionality;
  - **ecmascript modules** and use of modern **.mjs** extensions;
  - **[gulp4](https://www.npmjs.com/package/gulp/v/4.0.0#installation "link to installation instructions as it applies to gulp version four")** functionality;
  - **babeljs** functionality coupled with the use of **nomodule** attributes for **graceful degradation** purposes-- I still use an iPad2 with iOS v9 and presently **do not have the means** to splurge on a more modern version of iOS.
  - use of **gulp-ruby-sass** was decided on especially after it seemed that **node-sass** , as well as, **gulp-sass** were profoundly plagued by deprecation issues. The latter two were impossible to load, thus, I decided to get re&#45;acquainted with **gulp-ruby-sass**. 

## What were the biggest challenges?
First, the fact that **node-sass** was not going to install again&hellip;ever! Then, the babel compilation was simple enough, although, properly addressing the [regeneratorRuntime is not defined](https://stackoverflow.com/questions/41065943/regeneratorruntime-is-not-defined-can-i-fix-this-with-gulp "prescribed solution at stackoverflow") glitch was painful because 99 out of 100 responses were in the context of webpack and the like. I had to swallow my pride, go back several steps, then, incrementally address the issue.

## I thank each my Father and my Savior
My spirit couldn&rsquo;t be happier with the outcome even if I tried! Thank you for your visit! God bless you all.
