const gulp = require('gulp');

const concat = require('gulp-concat');
const uglify = require('gulp-uglify-es').default;
const rename = require('gulp-rename');

const postcss = require('gulp-postcss');

const autoprefixer = require('autoprefixer');

const imagemin = require( 'gulp-imagemin' );

const sourcemaps = require( 'gulp-sourcemaps' );
const newer = require( 'gulp-newer' );

const cssnano = require('cssnano');
const rollup = require('gulp-better-rollup'); 
const babel = require('gulp-babel');

const sass = require('gulp-sass'); 

const paths = { 
  
  imageSrc : [ 'images/**/*' ] , 
  imageDstFldr: 'img', 
  sassSrc : [ 'sass/**/*.scss' ] , 
  stylesSrc: 'styles/**/*.css' , 
  stylesFldr: 'styles', 
  cssDstFldr: 'css', 
  cssprelimNm: 'all.css', 
  cssDstFile: 'all.min.css', 

  modulesSrc: [ 'modules/**/*.?(m)js' ] , 
  moduleSrcPOE: 'modules/complete.mjs',

  jsSrc: [ 'js/polyfill.min.js', 'js/all.js' ] , 

  thirdPartyDestFold: 'third-party', 
  jsdestFold: 'js', 

  mjsdestFold: 'mjs', 
  mjsprelimNm: 'all.mjs', 
  mjsdestName: 'all.min.mjs',

  babelprelimName: 'all.js' ,
  babeldestName: 'all.min.js'
  
  , scriptsAxiosSrc: [ 
    'node_modules/axios/dist/axios.min.js'
    , 'node_modules/axios/dist/axios.min.map'
  ]

  , scriptsBabelPolyfillSrc: [ 
    'node_modules/@babel/polyfill/dist/polyfill.min.js'
  ] 

}; 

gulp.task( "imgminify" , function(done) {
  
  gulp.src( paths.imageSrc )
  .pipe( newer( paths.imageDstFldr ))
  .pipe( imagemin( { optimizationLevel: 3, progressive: true, interlaced: true } ) )
  .pipe( gulp.dest( paths.imageDstFldr ));
  
  done();

}); 

gulp.task( 'rollup-modules' , function(  ) {
  
  return gulp.src( paths.moduleSrcPOE )
  .pipe( sourcemaps.init() ) 
  .pipe( rollup(
    { 
      input: paths.moduleSrcPOE
    } , 
    { 
      format:'iife' 
    }
  ) ) 
  .pipe( concat( paths.mjsprelimNm ) )
  .pipe( rename( paths.mjsdestName ) )
  .pipe( uglify() )
  .pipe( sourcemaps.write() )
  .pipe( gulp.dest( paths.mjsdestFold ) );
  
} );

gulp.task( 'babelified-compilation', function() {
  
  return gulp.src('mjs/all.min.mjs')
  .pipe( sourcemaps.init() )
  .pipe( babel( { 
    filename: '.babelrc'
    , babelrcRoots: [
      "." 
    ]
  } ) )
  .pipe( rename( paths.babelprelimName ) )
  .pipe( uglify() )
  .pipe( sourcemaps.write( './', { includeContent: false, sourceRoot: paths.jsdestFold } ) )
  .pipe( gulp.dest( paths.jsdestFold ) );
  
} );

gulp.task( 'js-concatenation' , function() {
  return gulp.src( paths.jsSrc )
  .pipe( sourcemaps.init() )
  .pipe( concat( paths.babeldestName ) )
  .pipe( uglify() )
  .pipe( sourcemaps.write( './', { includeContent: false, sourceRoot: paths.jsdestFold } ) )
  .pipe( gulp.dest( paths.jsdestFold ) );

} );

// gulp-sass version
gulp.task( 'sass', function() {
  return gulp.src( paths.sassSrc )
    .pipe( sourcemaps.init() )
    .pipe( sass().on( 'error', sass.logError ) )
    .pipe( sourcemaps.write( './' , { includeContent: false , sourceRoot: paths.stylesFldr } ) )
    .pipe( gulp.dest( paths.stylesFldr ) );

});

gulp.task( 'styles', function() { 
  
  var plugins = [ 
    autoprefixer( { cascade: false } ) 
    , cssnano() 
  ];

  return gulp.src( paths.stylesSrc )
    .pipe( concat( paths.cssprelimNm ) )
    .pipe( rename( paths.cssDstFile ) )
    .pipe( postcss( plugins ) )
    .pipe( gulp.dest( paths.cssDstFldr ) );

});

gulp.task( 'importJsAxios' , function(done){
  
  gulp.src( paths.scriptsAxiosSrc )
  .pipe(gulp.dest(paths.thirdPartyDestFold) ); 

  done();
} );

gulp.task( 'importBabelPolyfill' , function(done){  
  
  gulp.src(paths.scriptsBabelPolyfillSrc)
  .pipe(gulp.dest(paths.jsdestFold) );
  
  done();
} );

gulp.task( "cssbuild", gulp.series( "sass", "styles") );

gulp.task( "modulebuild" , gulp.series( "rollup-modules" , "babelified-compilation", "js-concatenation" ) ); 

gulp.task( 
  'default' 
  , gulp.series( 'importJsAxios', 'importBabelPolyfill', 'modulebuild',  'cssbuild', 'imgminify' ) 
);